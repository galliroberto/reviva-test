README
==

https://dzone.com/articles/design-patterns-the-strategy-and-factory-patterns

## per usare docker
per prima cosa andare nella cartella docker e copare il file docker-compose.override.yaml.dist in docker-compose.override.yaml
al suo interno sono mappate le porte per i servizi
i paramentri di connessione possono non essere sovrascritti e lasciati quelli di default

### docker
per lanciare la 
```
./dc up
```

oppure detached
```
./dc up -d
```

se detached per fermare la docker
```
./dc stop
```

### per entrare nel container
```
./dc enter
```

### per installare i vendor
da dentro il container
```
composer install
```

### tests
da dentro il container
```
vendor/bin/phpunit --exclude demo
```

### esecuzione programma
da dentro il container
```
vendor/bin/phpunit --group demo
```

```
php cli.php PrintGoodListCommand run [cartId] 
cartId è un numero compreso tra 1 e 3
```


i dati delle transazioni sono presenti, per comodità:
- per il test, all'interno del test 
- per il commando, all'inteerno del comando 

la configurazione della dependency injection si trova in 
```
app/config/services.php
```

---
