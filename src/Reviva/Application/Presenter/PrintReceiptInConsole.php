<?php

namespace Reviva\Application\Presenter;

interface PrintReceiptInConsole
{
    public function execute(Cart $cart): void;
}