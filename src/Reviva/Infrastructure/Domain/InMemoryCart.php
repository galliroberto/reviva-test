<?php

namespace Reviva\Infrastructure\Domain;

use Reviva\Domain\Model\CartItem;
use Reviva\Domain\Model\Cart;

class InMemoryCart implements Cart
{
    private $cartItems = [];

    public function findAll(): array
    {
        return $this->cartItems;
    }

    public function findByName(string $name): array
    {
        return array_filter($this->cartItems, function (CartItem $cartItem) use ($name) {
            return $name === (string)$cartItem->good()->name();
        });
    }

    public function deleteAll(): void
    {
        $this->cartItems = [];
    }

    public function add(CartItem $cartItem): void
    {
        $this->cartItems[] = $cartItem;
    }
}
