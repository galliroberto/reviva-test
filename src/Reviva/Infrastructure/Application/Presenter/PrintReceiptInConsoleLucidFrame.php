<?php

namespace Reviva\Infrastructure\Application\Presenter;

use LucidFrame\Console\ConsoleTable;
use Reviva\Application\Service\GetTaxCalculator;
use Reviva\Domain\Model\Cart;
use Reviva\Domain\Service\CalculateGoodTax;
use Reviva\Domain\Service\RoundTotalTax;

class PrintReceiptInConsoleLucidFrame
{
    private CalculateGoodTax $calculateGoodTax;
    private RoundTotalTax $roundTotalTax;

    public function __construct(CalculateGoodTax $calculateGoodTax, RoundTotalTax $roundTotalTax)
    {
        $this->calculateGoodTax = $calculateGoodTax;
        $this->roundTotalTax = $roundTotalTax;
    }

    public function execute(Cart $cart): void
    {
        $this->printInput($cart);
        $this->printOuput($cart);
    }

    private function printInput(Cart $cart): void
    {
        echo sprintf("\nINPUT\n");

        $table = new ConsoleTable();
        $table
            ->addHeader('Qty')
            ->addHeader('Name')
            ->addHeader('Price');

        foreach ($cart->findAll() as $cartItem) {
            $table->addRow()
                ->addColumn($cartItem->quantity())
                ->addColumn($cartItem->good()->name())
                ->addColumn($this->printNumberWithTwoDecimal($cartItem->good()->price()->toFloat()));
        }
        $table->display();
    }

    private function printOuput(Cart $cart): void
    {
        echo sprintf("\nOUTPUT\n");

        $table = new ConsoleTable();
        $table
            ->addHeader('Qty')
            ->addHeader('Name')
            ->addHeader('Price');

        $salesTax = 0;
        $total = 0;
        foreach ($cart->findAll() as $cartItem) {
            $cartItemTotal = $cartItem->good()->price()->add(
                $this->calculateGoodTax->execute($cartItem->good())->toInt()
            )->toFloat();

            $table->addRow()
                ->addColumn($cartItem->quantity())
                ->addColumn($cartItem->good()->name())
                ->addColumn($this->printNumberWithTwoDecimal($cartItemTotal));

            $salesTax += $this->calculateGoodTax->execute($cartItem->good())->toFloat();
            $total += $cartItemTotal * $cartItem->quantity();
        }
        $table->display();

        $tableTotal = new ConsoleTable();
        $tableTotal->addRow()
            ->addColumn('Sales tax')
            ->addColumn($this->printNumberWithTwoDecimal($this->roundTotalTax->execute($salesTax)));

        $tableTotal->addRow()
            ->addColumn('Total')
            ->addColumn($this->printNumberWithTwoDecimal($total));

        $tableTotal->display();
    }


    private function printNumberWithTwoDecimal(float $number): string
    {
        return number_format($number, 2);
    }
}