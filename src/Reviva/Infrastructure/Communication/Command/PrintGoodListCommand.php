<?php

declare(strict_types=1);

namespace Reviva\Infrastructure\Communication\Command;

use Faker\Factory;
use Faker\Provider\it_IT\Address;
use LucidFrame\Console\ConsoleTable;
use Phalcon\Cli\Task;
use Reviva\Application\Presenter\PrintReceiptInConsole;
use Reviva\Application\Service\GetGoodsForReceipt;
use Reviva\Application\Service\ConvertiTransazioniInEuro;
use Reviva\Domain\Model\CartItem;
use Reviva\Domain\Model\Good;
use Reviva\Domain\ValueObject\Name;
use Reviva\Domain\ValueObject\Price;
use Reviva\Domain\ValueObject\Type;

class PrintGoodListCommand extends Task
{
    private GetGoodsForReceipt $ottieniTransazioniPerCliente;
    private ConvertiTransazioniInEuro $convertiTransazioniInEuro;
    private PrintReceiptInConsole $mostraLeTransazioniInConsole;

    public function onConstruct()
    {
        $this->cart = $this->getDi()->get('get-cart');
        $this->printReceipt = $this->getDi()->get('print-receipt');
    }

    public function mainAction()
    {
        echo 'Per lanciare lo script esegui php cli.php php cli.php PrintGoodList run [cartId]'.PHP_EOL;
    }

    public function runAction(int $cartId)
    {
        $this->feedCart($cartId);

        echo "\n";
        $this->printReceipt->execute($this->cart);
        echo "\n";
    }

    private function feedCart(int $cartId): void
    {
        $cartItems = [
            1 => [
                CartItem::create(
                    1,
                    Good::create(
                        Type::create(TYPE::GENERIC),
                        Name::create('Generic product 1'),
                        Price::create(1999),
                        false
                    )
                ),
                CartItem::create(
                    2,
                    Good::create(
                        Type::create(TYPE::GENERIC),
                        Name::create('Generic product 2'),
                        Price::create(2999),
                        false
                    )
                ),
                CartItem::create(
                    3,
                    Good::create(
                        Type::create(TYPE::GENERIC),
                        Name::create('Generic product 3'),
                        Price::create(599),
                        false
                    )
                ),
            ],
            2 => [
                CartItem::create(
                    2,
                    Good::create(
                        Type::create(TYPE::MEDICAL),
                        Name::create('Medical 1'),
                        Price::create(799),
                        false
                    )
                ),
                CartItem::create(
                    1,
                    Good::create(
                        Type::create(TYPE::MEDICAL),
                        Name::create('Medical imported 2'),
                        Price::create(299),
                        true
                    )
                ),
                CartItem::create(
                    4,
                    Good::create(
                        Type::create(TYPE::FOOD),
                        Name::create('Food 1'),
                        Price::create(2599),
                        false
                    )
                ),
            ],
            3 => [
                CartItem::create(
                    2,
                    Good::create(
                        Type::create(TYPE::BOOK),
                        Name::create('Book 1'),
                        Price::create(299),
                        true
                    )
                ),
                CartItem::create(
                    1,
                    Good::create(
                        Type::create(TYPE::FOOD),
                        Name::create('Food 1'),
                        Price::create(350),
                        true
                    )
                ),
                CartItem::create(
                    4,
                    Good::create(
                        Type::create(TYPE::FOOD),
                        Name::create('Food imported 2'),
                        Price::create(7500),
                        true
                    )
                ),
            ],
        ];

        foreach ($cartItems[$cartId] as $cartItem) {
            $this->cart->add($cartItem);
        }
    }
}