<?php

namespace Reviva\Infrastructure\Communication\Command;

class MainCommand extends Task
{
    public function mainAction()
    {
        echo 'This is the default task and the default action' . PHP_EOL;
    }

    public function runAction(int $param)
    {
        echo $param . PHP_EOL;
    }
}