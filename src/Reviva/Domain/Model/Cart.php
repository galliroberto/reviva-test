<?php

namespace Reviva\Domain\Model;

interface Cart
{
    public function findAll(): array;
    public function findByName(string $name): array;
    public function deleteAll(): void;
    public function add(CartItem $cartItem): void;
}
