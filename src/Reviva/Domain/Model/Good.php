<?php

namespace Reviva\Domain\Model;

use DateTimeImmutable;
use Reviva\Domain\Aggregate\Misura\Misure;
use Reviva\Domain\ValueObject\MisurazioneParametri;
use Reviva\Domain\ValueObject\ClienteId;
use Reviva\Domain\ValueObject\Type;
use Reviva\Domain\ValueObject\Name;
use Reviva\Domain\ValueObject\Price;

final class Good
{
    private Type $type;
    private Name $name;
    private Price $price;
    private bool $isImported;

    private function __construct(
        Type $type,
        Name $name,
        Price $price,
        bool $isImported
    ) {
        $this->type = $type;
        $this->name = $name;
        $this->price = $price;
        $this->isImported = $isImported;
    }

    public static function create(
        Type $type,
        Name $name,
        Price $price,
        bool $isImported
    ): self {
        return new self($type, $name, $price, $isImported);
    }

    public function type(): Type
    {
        return $this->type;
    }

    public function name(): Name
    {
        return $this->name;
    }
    
    public function price(): Price
    {
        return $this->price;
    }

    public function isImported(): bool
    {
        return $this->isImported;
    }
}