<?php

namespace Reviva\Domain\Model;

use DateTimeImmutable;
use Reviva\Domain\Aggregate\Misura\Misure;
use Reviva\Domain\ValueObject\MisurazioneParametri;
use Reviva\Domain\ValueObject\ClienteId;
use Reviva\Domain\ValueObject\Type;
use Reviva\Domain\ValueObject\Name;
use Reviva\Domain\ValueObject\Price;

final class CartItem
{
    private int $quantity;
    private Good $good;

    private function __construct(
        int $quantity,
        Good $good
    ) {
        $this->quantity = $quantity;
        $this->good = $good;
    }

    public static function create(
        int $quantity,
        Good $good
    ): self {
        return new self($quantity, $good);
    }

    public function quantity(): int
    {
        return $this->quantity;
    }

    public function good(): Good
    {
        return $this->good;
    }

    public function total(): Price
    {
        return Price::create($this->quantity * $this->good()->price()->toInt());
    }

}