<?php

declare(strict_types=1);

namespace Reviva\Domain\ValueObject;

use Common\ValueObject\ValidationException;

final class Name
{
    private string $name;

    private function __construct(string $name)
    {
        $this->validateNameOrFail($name);
        $this->name = $name;
    }

    public static function create(string $name): self
    {
        return new self(trim($name));
    }

    private function validateNameOrFail(string $name): void
    {
        if (!preg_match('/^[A-Za-z0-9 _-]*$/', $name)) {
            throw new ValidationException(
                sprintf('Name is not valid [%s]. We accept only string with [A-Za-z0-9 _-]', $name)
            );
        }
    }

    public function name(): string
    {
        return $this->name;
    }

    public function toArray(): array
    {
        return [
            'name' => $this->name,
        ];
    }

    public function __toString(): string
    {
        return $this->name;
    }

    public function __toClone(): void
    {
    }
}