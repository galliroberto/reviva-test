<?php

declare(strict_types=1);

namespace Reviva\Domain\ValueObject;

use Common\ValueObject\ValidationException;
use InvalidArgumentException;

final class Type
{
    const GENERIC = 'GENERIC';
    const FOOD = 'FOOD';
    const BOOK = 'BOOK';
    const MEDICAL = 'MEDICAL';

    private array $validType = [
        self::GENERIC,
        self::FOOD,
        self::BOOK,
        self::MEDICAL,
    ];

    private string $type;

    private function __construct(string $type)
    {
        $this->validateTypeOrFail($type);
        $this->type = $type;
    }

    public static function create(string $type): self
    {
        return new self(strtoupper(trim($type)));
    }

    private function validateTypeOrFail(string $type): void
    {
        if (!in_array($type, $this->validType)) {
            throw new ValidationException(
                sprintf('Type is not valid [%s]. We accept [%s]', $type, implode(', ', $this->validType))
            );
        }
    }

    public function type(): string
    {
        return $this->type;
    }

    public function toArray(): array
    {
        return [
            'type' => $this->type,
        ];
    }

    public function __toString(): string
    {
        return $this->type;
    }

    public function __toClone(): void
    {
    }
}