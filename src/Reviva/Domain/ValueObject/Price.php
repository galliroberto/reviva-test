<?php

declare(strict_types=1);

namespace Reviva\Domain\ValueObject;

use Common\ValueObject\ValidationException;

final class Price
{
    private int $price;

    private function __construct(int $price)
    {
        $this->validatePriceOrFail($price);
        $this->price = $price;
    }

    public static function create(int $price): self
    {
        return new self($price);
    }

    private function validatePriceOrFail(int $price): void
    {
        if ($price < 0) {
            throw new ValidationException(
                sprintf('Price is not valid [%s]. We accept only integer.', $price)
            );
        }
    }

    public function price(): int
    {
        return $this->price;
    }

    public function getPercent(int $percent): self
    {
        return Price::create((int) ($this->price / 100 * $percent));
    }

    public function add(int $addValue): self
    {
        return Price::create($this->price + $addValue);
    }

    public function toArray(): array
    {
        return [
            'price' => $this->price,
        ];
    }

    public function toInt(): int
    {
        return (int)$this->price;
    }

    public function toFloat(): float
    {
        return (float) $this->price / 100;
    }

    public function __toString(): string
    {
        return (string)$this->price;
    }

    public function __toClone(): void
    {
    }
}