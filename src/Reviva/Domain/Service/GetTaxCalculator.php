<?php

namespace Reviva\Domain\Service;

use Reviva\Domain\Service\TaxCalculator\BasicTaxCalculator;
use Reviva\Domain\Service\TaxCalculator\ImportedTaxCalculatorDecorator;
use Reviva\Domain\Service\TaxCalculator\NoTaxCalculator;
use Reviva\Domain\Service\TaxCalculator\TaxCalculator;
use Reviva\Domain\Model\Good;
use Reviva\Domain\ValueObject\Type;

final class GetTaxCalculator
{
    public function execute(Good $good): TaxCalculator
    {
        switch ($good->type()->type()) {
            case Type::GENERIC:
                $taxCalculator = new BasicTaxCalculator();
                break;
            default:
                $taxCalculator = new NoTaxCalculator();
        }

        if($good->isImported()) {
            $taxCalculator = new ImportedTaxCalculatorDecorator($taxCalculator);
        }

        return $taxCalculator;
    }
}