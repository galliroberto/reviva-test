<?php

namespace Reviva\Domain\Service\TaxCalculator;

use Reviva\Domain\ValueObject\Price;

class BasicTaxCalculator implements TaxCalculator
{
    private const TAX_PERCENT = 10;

    public function execute(Price $price): Price
    {
        return $price->getPercent(self::TAX_PERCENT);
    }
}