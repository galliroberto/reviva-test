<?php

namespace Reviva\Domain\Service\TaxCalculator;

use Reviva\Domain\ValueObject\Price;

interface TaxCalculator
{
    public function execute(Price $price): Price;
}