<?php

namespace Reviva\Domain\Service\TaxCalculator;

use Reviva\Domain\ValueObject\Price;

final class ImportedTaxCalculatorDecorator implements TaxCalculator
{
    private const TAX_PERCENT = 5;
    private TaxCalculator $taxCalculator;

    public function __construct(TaxCalculator $taxCalculator) {
        $this->taxCalculator = $taxCalculator;
    }

    public function execute(Price $price): Price
    {
        return $price->getPercent(self::TAX_PERCENT)->add(
            $this->taxCalculator->execute($price)->toInt()
        );
    }
}