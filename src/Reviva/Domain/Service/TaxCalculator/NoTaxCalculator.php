<?php

namespace Reviva\Domain\Service\TaxCalculator;

use Reviva\Domain\ValueObject\Price;

class NoTaxCalculator implements TaxCalculator
{
    private const TAX_PERCENT = 0;

    public function execute(Price $price): Price
    {
        return Price::create(self::TAX_PERCENT);
    }
}