<?php

namespace Reviva\Domain\Service;

class RoundTotalTax
{
    public function execute(float $salesTax): float
    {
        return ceil($salesTax / 0.05) * 0.05;
    }
}