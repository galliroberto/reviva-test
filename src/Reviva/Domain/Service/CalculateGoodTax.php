<?php

namespace Reviva\Domain\Service;

use Reviva\Domain\Service\GetTaxCalculator;
use Reviva\Domain\Model\Good;
use Reviva\Domain\ValueObject\Price;

class CalculateGoodTax
{
    private GetTaxCalculator $getTaxCalculator;

    public function __construct(GetTaxCalculator $getTaxCalculator)
    {
        $this->getTaxCalculator = $getTaxCalculator;
    }

    public function execute(Good $good): Price
    {
        $taxCalculator = $this->getTaxCalculator->execute($good);

        return $taxCalculator->execute($good->price());
    }
}