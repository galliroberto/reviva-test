<?php

namespace Common\ValueObject;

use Exception;
use Throwable;

class ValidationException extends Exception
{
    public function __construct($message, $code = 400, Throwable $previous = null) {

        parent::__construct($message, $code, $previous);
    }

    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}