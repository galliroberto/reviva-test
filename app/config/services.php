<?php

declare(strict_types=1);

use Reviva\Domain\Service\CalculateGoodTax;
use Reviva\Domain\Service\GetTaxCalculator;
use Reviva\Domain\Service\RoundTotalTax;
use Reviva\Infrastructure\Application\Presenter\PrintReceiptInConsoleLucidFrame;
use Reviva\Infrastructure\Domain\InMemoryCart;
use Reviva\Infrastructure\Domain\PhalconCart;

$container->set(
    'get-cart',
    [
        'className' => InMemoryCart::class,
        'arguments' => [
            [
                'type' => 'parameter',
                'name' => 'inMemoryCart',
                'value' => new InMemoryCart(),
            ],
        ]
    ]
);

$container->set(
    'print-receipt',
    [
        'className' => PrintReceiptInConsoleLucidFrame::class,
        'arguments' => [
            [
                'type' => 'parameter',
                'name' => 'CalculateGoodTax',
                'value' => new CalculateGoodTax(new GetTaxCalculator()),
            ],
            [
                'type' => 'parameter',
                'name' => 'RoundTotalTax',
                'value' => new RoundTotalTax(),
            ],
        ]
    ]
);

