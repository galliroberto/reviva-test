<?php

namespace Reviva;

use PHPUnit\Framework\TestCase;
use Reviva\Domain\Service\GetTaxCalculator;
use Reviva\Domain\Model\CartItem;
use Reviva\Domain\Model\Good;
use Reviva\Domain\Service\CalculateGoodTax;
use Reviva\Domain\Service\RoundTotalTax;
use Reviva\Domain\ValueObject\Name;
use Reviva\Domain\ValueObject\Price;
use Reviva\Domain\ValueObject\Type;
use Reviva\Infrastructure\Application\Presenter\PrintReceiptInConsoleLucidFrame;
use Reviva\Infrastructure\Domain\InMemoryCart;

class DemoTest extends TestCase
{
    /**
     * @test
     * @group        demo
     * @dataProvider generic_goods_not_imported_provider
     */
    public function print_receipt_generic_goods_not_imported(array $cartItems): void
    {
        $cart = new InMemoryCart();
        $printReceipt = new PrintReceiptInConsoleLucidFrame(
            new CalculateGoodTax(new GetTaxCalculator()),
            new RoundTotalTax()
        );

        foreach ($cartItems as $cartItem) {
            $cart->add($cartItem);
        }

        echo "\n";
        $printReceipt->execute($cart);
        echo "\n";

        $this->assertTrue(true);
    }

    public function generic_goods_not_imported_provider(): array
    {
        return [
            [
                [
                    CartItem::create(
                        1,
                        Good::create(
                            Type::create(TYPE::GENERIC),
                            Name::create('Generic product 1'),
                            Price::create(1999),
                            false
                        )
                    ),
                    CartItem::create(
                        2,
                        Good::create(
                            Type::create(TYPE::GENERIC),
                            Name::create('Generic product 2'),
                            Price::create(2999),
                            false
                        )
                    ),
                    CartItem::create(
                        3,
                        Good::create(
                            Type::create(TYPE::GENERIC),
                            Name::create('Generic product 3'),
                            Price::create(599),
                            false
                        )
                    ),
                ],
            ],
            [
                [
                    CartItem::create(
                        2,
                        Good::create(
                            Type::create(TYPE::MEDICAL),
                            Name::create('Medical 1'),
                            Price::create(799),
                            false
                        )
                    ),
                    CartItem::create(
                        1,
                        Good::create(
                            Type::create(TYPE::MEDICAL),
                            Name::create('Medical imported 2'),
                            Price::create(299),
                            true
                        )
                    ),
                    CartItem::create(
                        4,
                        Good::create(
                            Type::create(TYPE::FOOD),
                            Name::create('Food 1'),
                            Price::create(2599),
                            false
                        )
                    ),
                ],
            ],
            [
                [
                    CartItem::create(
                        2,
                        Good::create(
                            Type::create(TYPE::BOOK),
                            Name::create('Book 1'),
                            Price::create(299),
                            true
                        )
                    ),
                    CartItem::create(
                        1,
                        Good::create(
                            Type::create(TYPE::FOOD),
                            Name::create('Food 1'),
                            Price::create(350),
                            true
                        )
                    ),
                    CartItem::create(
                        4,
                        Good::create(
                            Type::create(TYPE::FOOD),
                            Name::create('Food imported 2'),
                            Price::create(7500),
                            true
                        )
                    ),
                ],
            ],
        ];
    }
}