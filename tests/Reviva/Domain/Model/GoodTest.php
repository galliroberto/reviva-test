<?php

namespace Tests\Reviva\Domain\Model;

use Reviva\Domain\Model\Good;
use PHPUnit\Framework\TestCase;
use Reviva\Domain\ValueObject\Name;
use Reviva\Domain\ValueObject\Price;
use Reviva\Domain\ValueObject\Type;

class GoodTest extends TestCase
{
    /**
     * @test
     * @dataProvider valid_good_provider
     */
    public function create_valid_good(string $type, string $name, int $price, bool $isImported): void
    {
        $good = Good::create(
            Type::create($type),
            Name::create($name),
            Price::create($price),
            $isImported
        );
        $this->assertEquals($type, $good->type());
        $this->assertEquals($name, $good->name());
        $this->assertEquals($price, $good->price()->price());
        $this->assertEquals($isImported, $good->isImported());
    }

    public function valid_good_provider(): array
    {
        return [
            [
                'GENERIC',
                'test name',
                1550,
                false
            ],
        ];
    }
}
