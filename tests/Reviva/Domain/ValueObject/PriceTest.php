<?php

namespace Tests\Reviva\Domain\ValueObject;

use Common\ValueObject\ValidationException;
use InvalidArgumentException;
use Reviva\Domain\ValueObject\Price;
use PHPUnit\Framework\TestCase;

class PriceTest extends TestCase
{
    /**
     * @test
     * @dataProvider valid_price_provider
     */
    public function create_valid_price(int $priceProvider): void
    {
        $this->assertEquals($priceProvider, Price::create($priceProvider)->price());
    }

    public function valid_price_provider(): array
    {
        return [
            [0],
            [99],
            [900],
            [1550],
            [2550],
            [3550],
            [1551],
            [2552],
            [3553],
        ];
    }

    /**
     * @test
     * @dataProvider invalid_price_provider
     */
    public function create_invalid_price(int $priceProvider): void
    {
        $this->expectException(ValidationException::class);

        Price::create($priceProvider);
    }

    public function invalid_price_provider(): array
    {
        return [
            [-1],
        ];
    }
}
