<?php

namespace Tests\Reviva\Domain\ValueObject;

use Common\ValueObject\ValidationException;
use Reviva\Domain\ValueObject\Name;
use PHPUnit\Framework\TestCase;

class NameTest extends TestCase
{
    /**
     * @test
     * @dataProvider valid_name_provider
     */
    public function create_valid_name(string $nameProvider): void
    {
        $this->assertEquals($nameProvider, Name::create($nameProvider)->name());
    }

    public function valid_name_provider(): array
    {
        return [
            ['product 1'],
            ['zebra test'],
            ['123 565'],
        ];
    }

    /**
     * @test
     * @dataProvider invalid_name_provider
     */
    public function create_invalid(string $nameProvider): void
    {
        $this->expectException(ValidationException::class);

        Name::create($nameProvider);
    }

    public function invalid_name_provider(): array
    {
        return [
            ['prova &'],
            ['&15'],
            ['?25'],
            ['£35.qualcosa'],
            ['&15.5'],
            ['?25.5'],
            ['€test'],
            ['$test'],
        ];
    }
}
