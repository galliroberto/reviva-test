<?php

namespace Tests\Reviva\Support\Builder;

use Reviva\Domain\Model\Good;
use Reviva\Domain\ValueObject\Name;
use Reviva\Domain\ValueObject\Price;
use Reviva\Domain\ValueObject\Type;

class GoodBuilder
{
    private Type $type;
    private Name $name;
    private Price $price;
    private bool $isImported;

    protected function __construct()
    {
        $this->type = TypeBuilder::create()->build();
        $this->name = NameBuilder::create()->build();
        $this->price = PriceBuilder::create()->build();
        $this->isImported = false;
    }

    public static function create(): self
    {
        return new self();
    }

    public function withType(Type $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function withName(Name $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function withPrice(Price $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function withIsImported(bool $isImported): self
    {
        $this->isImported = $isImported;

        return $this;
    }

    public function build(): Good
    {
        return Good::create(
            $this->type,
            $this->name,
            $this->price,
            $this->isImported
        );
    }
}
