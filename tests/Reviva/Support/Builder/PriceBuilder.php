<?php

namespace Tests\Reviva\Support\Builder;

use Reviva\Domain\ValueObject\Price;

class PriceBuilder
{
    private int $price;

    protected function __construct()
    {
        $this->price = 1900;
    }

    public static function create(): self
    {
        return new static();
    }

    public function withPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function build(): Price
    {
        return Price::create($this->price);
    }
}