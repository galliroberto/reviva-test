<?php

namespace Tests\Reviva\Support\Builder;

use Reviva\Domain\ValueObject\Type;

class TypeBuilder
{
    private string $type;

    protected function __construct()
    {
        $this->type = 'GENERIC';
    }

    public static function create(): self
    {
        return new static();
    }

    public function withType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function build(): Type
    {
        return Type::create($this->type);
    }
}