<?php

namespace Tests\Reviva\Support\Builder;

use Reviva\Domain\ValueObject\Name;

class NameBuilder
{
    private string $name;

    protected function __construct()
    {
        $this->name = 'Prodotto base';
    }

    public static function create(): self
    {
        return new static();
    }

    public function withName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function build(): Name
    {
        return Name::create($this->name);
    }
}
