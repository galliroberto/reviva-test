<?php

namespace Tests\Reviva\Application\Service;

use PHPUnit\Framework\TestCase;
use Reviva\Domain\Model\CartItem;
use Reviva\Domain\ValueObject\Name;
use Reviva\Infrastructure\Domain\InMemoryCart;
use Tests\Reviva\Support\Builder\GoodBuilder;

class FindCartItemByNameTest extends TestCase
{
    /**
     * @test
     * @dataProvider cart_items_provider
     */
    public function get_all_cart_items(array $cartItemsProvider): void
    {
        $cart = new InMemoryCart();
        foreach ($cartItemsProvider as $cartItem) {
            $cart->add($cartItem);
        }

        $this->assertCount(0, $cart->findByName('not exists'));
        $this->assertCount(1, $cart->findByName('I exist'));
        $this->assertCount(1, $cart->findAll());
    }

    public function cart_items_provider(): array
    {
        return [
            [
                [
                    CartItem::create(1, GoodBuilder::create()->withName(Name::create('I exist'))->build())
                ],
            ],
        ];
    }
}
