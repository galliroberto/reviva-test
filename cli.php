<?php

declare(strict_types=1);

require __DIR__ . '/vendor/autoload.php';

use Phalcon\Cli\Console;
use Phalcon\Cli\Dispatcher;
use Phalcon\Di\FactoryDefault\Cli as CliDI;
use Phalcon\Loader;
use Reviva\Application\Util\EnvUtil;
use Symfony\Component\Dotenv\Dotenv;
use Reviva\Infrastructure\Domain\InMemoryTransazioni;

define('BASE_PATH', dirname(__DIR__) . '/app/');
define('APP_PATH', BASE_PATH . 'app/');

$container = new CliDI();
$dispatcher = new Dispatcher();

$dispatcher->setDefaultNamespace('Reviva\Infrastructure\Communication\Command');
$dispatcher->setNamespaceName('Reviva\Infrastructure\Communication\Command');
$container->setShared('dispatcher', $dispatcher);
$dispatcher->setTaskSuffix('Command');
//$dispatcher->setDefaultTask('fixture');


require APP_PATH . 'config/services.php';
/*
$container->set(
    'name-service',
    [
        'className' => CLASSNAME::class,
        'arguments' => [
            [
                'type' => 'parameter',
                'name' => 'goods',
                'value' => new InMemoryGoods(),
            ],
        ]
    ]
);
*/

$console = new Console($container);

$arguments = [];
foreach ($argv as $k => $arg) {
    if ($k === 1) {
        $arguments['task'] = $arg;
    } elseif ($k === 2) {
        $arguments['action'] = $arg;
    } elseif ($k >= 3) {
        $arguments['params'][] = $arg;
    }
}

try {
    $console->handle($arguments);
} catch (Throwable $throwable) {
    fwrite(STDERR, $throwable->getMessage() . PHP_EOL);
    exit(1);
}